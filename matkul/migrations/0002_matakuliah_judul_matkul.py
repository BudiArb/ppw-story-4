# Generated by Django 3.1.2 on 2020-10-17 06:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('matkul', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='matakuliah',
            name='judul_matkul',
            field=models.CharField(default='Judul', max_length=20),
        ),
    ]
