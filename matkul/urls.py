from django.urls import path

from .views import index, tambah_matkul, detail, hapus_matkul

urlpatterns = [
    path('', index, name='index'),
    path('tambah', tambah_matkul, name='tambah'),
    path('detail/<str:matkul_id>', detail, name='detail'),
    path('hapus/<str:matkul_id>', hapus_matkul, name='hapus'),
]