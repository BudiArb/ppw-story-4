from django import forms
from .models import MataKuliah

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = '__all__'
        widgets = {
            'judul_matkul' : forms.TextInput(attrs={
                'class' : 'form-input'
            }), 
            'nama_dosen' : forms.TextInput(attrs={
                'class' : 'form-input'
            }), 
            'jumlah_sks' : forms.NumberInput(attrs={
                'class' : 'form-input'
            }),
            'deskripsi' : forms.TextInput(attrs={
                'class' : 'form-input'
            }),
            'semester_tahun' : forms.TextInput(attrs={
                'class' : 'form-input'
            }),
            'ruang_kelas' : forms.TextInput(attrs={
                'class' : 'form-input'
            }),
        }