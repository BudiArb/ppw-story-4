from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import MatkulForm

from .models import MataKuliah

def index(request):
    matkul_list = MataKuliah.objects.all()
    return render(request, 'matkul/index.html', {'matkul_list' : matkul_list})

def tambah_matkul(request):
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/matkul')
    else:
        form = MatkulForm()
    return render(request, 'matkul/tambah.html', {'form' : form})

def detail(request, matkul_id):
    matakuliah = MataKuliah.objects.get(id=str(matkul_id))
    
    return render(request, 'matkul/detail.html', {'matkul' : matakuliah})

def hapus_matkul(request, matkul_id):
    matakuliah = MataKuliah.objects.get(id=str(matkul_id))
    matakuliah.delete()
    
    return HttpResponseRedirect('/matkul')